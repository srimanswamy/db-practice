import jakarta.servlet.http.*;
import jakarta.servlet.*;
import org.json.simple.JSONObject;
import java.io.*;
import java.util.*;


public class DataDemo extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        res.setContentType("text/html");
        PrintWriter pw = res.getWriter();
        String output = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Hello World</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<form action = 'demo' method = \"GET\">\n" +
                "    Value Read: <input type = text name = \"dataString\" value = \"";
        Scanner inp = new Scanner(new File("D:\\Tomcat Practice\\demo.txt"));
        if(inp.hasNextLine())
        {
            output += ""+inp.nextLine();
        }
        else
        {
            output += "Nothing found. Post a value";
        }
        output += "\"></input>\n" +
                "    <br>\n" +
                "    <input type = submit value = 'Retrieve'></input>\n" +
                "</form>\n" +
                "\n" +
                "<form action = 'demo' method = \"POST\">\n" +
                "    Post Value: <input type = text name = \"dataString\"></input>\n" +
                "    <br>\n" +
                "    <input type = submit value = \"Post Value\"></input>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
        pw.print(output);
        pw.close();//closing the stream
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        String data = req.getParameter("dataString");
//        JsonObject obj = new JsonObject();
//        obj.put("dataString", data);
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("D:\\Tomcat Practice\\demo.txt")));
        if(data != null && !data.equals(""))
        {
            writer.append(data);
        }
        else
        {
            PrintWriter pw = res.getWriter();
            pw.print("<br>Warning: No value entered<br>Enter one and hit Post<br>");
        }
        writer.close();
        this.doGet(req, res);
    }
}  