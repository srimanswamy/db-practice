import jakarta.servlet.http.*;
import jakarta.servlet.*;
import java.io.*;
//import java.util.*;
public class SubServlet extends HttpServlet {
    public void doGet(HttpServletRequest req,HttpServletResponse res)
            throws ServletException,IOException {

        res.setContentType("text/html");
        PrintWriter pw = res.getWriter();

        //writing html in the stream
        pw.println("<html><title>Subtract Two Numbers</title><body>");
        pw.println("<form action=\"sub\" method=POST>First Number: <input type=text size=20 name=firstnum><br>Second Number: <input type=text size=20 name=secnum><br><input type=submit></form>");
        pw.println("</body></html>");
        pw.close();

    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {

        String op1 = req.getParameter("firstnum");
        String op2 = req.getParameter("secnum");

        int diff = 0;
        if(op1 != null && op2 != null) {
            int fir = Integer.parseInt(op1);
            int sec = Integer.parseInt(op2);
            diff = fir - sec;
        }
        PrintWriter pw = res.getWriter();
        pw.println("<html><body>Difference = "+diff+" </body></html>");
        pw.close();

        doGet(req, res);
    }
}