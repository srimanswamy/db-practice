import jakarta.servlet.http.*;
import jakarta.servlet.*;
import org.json.*;
import org.apache.commons.text.*;

import java.io.*;
import java.util.*;
import java.sql.*;

public class ApiDB extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try (
                Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "sriman");
        ) {
            int empid = Integer.parseInt(req.getParameter("empid"));

            PreparedStatement stmt = conn.prepareStatement("select salary from employee where empid = ?");
            stmt.setInt(1, empid);
            ResultSet rs = stmt.executeQuery();
            if (!rs.next()) {
                res.getWriter().print("Employee not found");
            } else {
                res.getWriter().print("<html>" + StringEscapeUtils.escapeHtml4("Employee ID = " + empid + "; Salary = " + rs.getInt(1)) + "</html>");
            }
        } catch (NumberFormatException e) {
            res.getWriter().print("Inputs should be Integers");
        } catch (Exception e) {
            res.getWriter().print("Caught the exception");
        }
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try (
                Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "sriman");
        ) {
            int empid = Integer.parseInt(req.getParameter("empid"));
            int sal = Integer.parseInt(req.getParameter("salary"));

            PreparedStatement stmt = conn.prepareStatement("select exists (select * from employee where empid = ?)");
            stmt.setInt(1, empid);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            if (!rs.getBoolean(1)) {

                //if not already exists, insert
                stmt = conn.prepareStatement("insert into employee values (?, ?)");
                stmt.setInt(1, empid);
                stmt.setInt(2, sal);
                int rows = stmt.executeUpdate();
                if (rows == 1) {
                    res.getWriter().print("Success");
                } else {
                    res.getWriter().print("Unable to write to DB");
                }
            } else {
                //if already exists, tell em
                res.getWriter().print("Already Exists!! Use PUT to update");
            }
        } catch (NumberFormatException e) {
            res.getWriter().print("Inputs should be Integers");
        } catch (Exception e) {
            res.getWriter().print("Caught the exception");
        }
    }

    public void doDelete(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try(
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "sriman");
        ){
            int empid = Integer.parseInt(req.getParameter("empid"));

            PreparedStatement stmt = conn.prepareStatement("delete from employee where empid = ?");
            stmt.setInt(1, empid);
            int rows = stmt.executeUpdate();
            if (rows == 0) {
                res.getWriter().print("Employee not found");
            } else if (rows == 1) {
                res.getWriter().print("Success");
            } else {
                res.getWriter().print("More than one record present, deleted all");
            }
        }catch(NumberFormatException e){
            res.getWriter().print("Inputs should be Integers");
        } catch(Exception e){
            res.getWriter().print("Caught the exception");
        }
}


    public void doPut(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try (
                Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "sriman");
        ) {
            int empid = Integer.parseInt(req.getParameter("empid"));
            int sal = Integer.parseInt(req.getParameter("salary"));

            PreparedStatement stmt = conn.prepareStatement("update employee set salary = ? where empid = ?");
            stmt.setInt(1, sal);
            stmt.setInt(2, empid);
            int rows = stmt.executeUpdate();
            if (rows == 0) {
                res.getWriter().print("No employee found!! Use POST to create");
            } else if (rows == 1) {
                res.getWriter().print("Success");
            } else {
                res.getWriter().print("More than one record present, updated all");
            }
        } catch(NumberFormatException e){
            res.getWriter().print("Inputs should be Integers");
        }catch(Exception e){
            res.getWriter().print("Caught the exception");
        }
     }
    }