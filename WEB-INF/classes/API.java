import jakarta.servlet.http.*;
import jakarta.servlet.*;
import org.json.*;
import org.apache.commons.text.*;
import java.io.*;
import java.util.*;

public class API extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try {
            int i = Integer.parseInt(req.getParameter("index"));
            File fl = new File("D:\\Tomcat Practice\\api.json");
            fl.createNewFile();
            Scanner inp = new Scanner(fl);
            String content = "";
            while (inp.hasNextLine()) {
                content += inp.nextLine();
            }
            inp.close();

            if (content.equals("")) {
                content += "{}";
            }
            JSONObject obj = new JSONObject(content);
            if (!obj.has("stringArray")) {
                res.getWriter().print("Index Not Present");
                return;
            }
            JSONArray arr = obj.getJSONArray("stringArray");
            if (arr.length() < i || i < 1) {
                res.getWriter().print("Index Not Present");
                return;
            }
            res.getWriter().print("<html>"+StringEscapeUtils.escapeHtml4(arr.get(i - 1)+"")+"</html>");
//            res.getWriter().print(StringEscapeUtils.escapeHtml4(arr.get(i - 1)+""));
        } catch (Exception e) {
            res.getWriter().print("Caught the exception: "+e);
        }
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try {
            String val = req.getParameter("value");
            if (val == null || val.equals("")) {
                res.getWriter().print("Invalid input");
                return;
            }
            File fl = new File("D:\\Tomcat Practice\\api.json");
            fl.createNewFile();
            Scanner inp = new Scanner(fl);
            String content = "";
            while (inp.hasNextLine()) {
                content += inp.nextLine();
            }
            inp.close();

            if (content.equals("")) {
                content += "{}";
            }
            JSONObject obj = new JSONObject(content);
            if (!obj.has("stringArray")) {
                obj.put("stringArray", new JSONArray());
            }
            obj.getJSONArray("stringArray").put(val);
            FileWriter file = new FileWriter(fl);
            file.write(obj.toString());
            file.close();
            res.getWriter().print("Success");
        } catch (Exception e) {
            res.getWriter().print("Caught the exception: "+e);
        }
    }

    public void doDelete(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try {
            int i = Integer.parseInt(req.getParameter("index"));
            File fl = new File("D:\\Tomcat Practice\\api.json");
            fl.createNewFile();
            Scanner inp = new Scanner(fl);
            String content = "";
            while (inp.hasNextLine()) {
                content += inp.nextLine();
            }
            inp.close();

            if (content.equals("")) {
                content += "{}";
            }
            JSONObject obj = new JSONObject(content);
            if (!obj.has("stringArray")) {
                res.getWriter().print("Index Not Present");
                return;
            }
            JSONArray arr = obj.getJSONArray("stringArray");
            if (arr.length() < i || i < 1) {
                res.getWriter().print("Index Not Present");
                return;
            }
            String rem = arr.remove(i - 1).toString();
            FileWriter file = new FileWriter(fl);
            file.write(obj.toString());
            file.close();
            res.getWriter().print("<pre>"+StringEscapeUtils.escapeHtml4("Deleted: " + rem)+"</pre>");
        } catch (Exception e) {
            res.getWriter().print("Caught the exception: "+e);
        }
    }

    public void doPut(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        try {
            int i = Integer.parseInt(req.getParameter("index"));
            String val = req.getParameter("value");
            File fl = new File("D:\\Tomcat Practice\\api.json");
            fl.createNewFile();
            Scanner inp = new Scanner(fl);
            String content = "";
            while (inp.hasNextLine()) {
                content += inp.nextLine();
            }
            inp.close();

            if (content.equals("")) {
                content += "{}";
            }
            JSONObject obj = new JSONObject(content);
            if (!obj.has("stringArray")) {
                res.getWriter().print("Index Not Present");
                return;
            }
            JSONArray arr = obj.getJSONArray("stringArray");
            if (arr.length() < i || i < 1) {
                res.getWriter().print("Index Not Present");
                return;
            }
            arr.put(i - 1, val);
            FileWriter file = new FileWriter(fl);
            file.write(obj.toString());
            file.close();
            res.getWriter().print("Success");
        } catch (Exception e) {
            res.getWriter().print("Caught the exception: "+e);
        }
    }
}