import java.sql.*;
class DB
{
    public static void main(String[] args) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "sriman");
        //Get
            PreparedStatement stmt = conn.prepareStatement("select salary from employee where empid = ?");
            stmt.setInt(1, 3);
            ResultSet rs = stmt.executeQuery();
            if(!rs.next())
            {
                System.out.println("Employee not found");
            }
            else
            {
                System.out.println("Employee ID = "+1+"; Salary = "+rs.getInt(1));
            }
        }catch(Exception e)
        {
            System.out.println(e);
        }
    }
}