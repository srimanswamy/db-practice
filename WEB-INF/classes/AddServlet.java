import jakarta.servlet.http.*;
import jakarta.servlet.*;

import java.io.*;

//import java.util.*;
public class AddServlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException {
        res.setContentType("text/html");
        PrintWriter pw = res.getWriter();

        //writing html in the stream
        pw.println("<html><head>Add Two Numbers</head><body>");
        pw.println("<form action=\"add\" method=GET>First Number: <input type=text size=20 name=firstnum><br>Second Number: <input type=text size=20 name=secnum><br><input type=submit></form>");
        String op1 = req.getParameter("firstnum");
        String op2 = req.getParameter("secnum");
        if (op1 != null && op2 != null) {
            int fir = Integer.parseInt(op1);
            int sec = Integer.parseInt(op2);
            int sum = fir + sec;
            pw.print("Sum = " + sum);
        } else {
            pw.print("Enter Values");
        }
        pw.println("</body></html>");
        pw.close();//closing the stream
    }
//
//    public void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws IOException, ServletException {
//        doGet(request, response);
//    }
}  